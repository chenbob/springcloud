package com.sdnware.compute;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ComputeServRunApplication {
	public static void main(String[] args) {
		new SpringApplicationBuilder(ComputeServRunApplication.class).web(true).run(args);
	}

}
